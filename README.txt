This module allows the default Drupal  maintenance mode setting to override the
setting provided by domain_conf. This is useful if your deployment process
enables maintenance mode via drush. Normally with domain_conf the individual
domain setting would override the Drupal default - this module reverses that.

